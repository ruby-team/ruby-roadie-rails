ruby-roadie-rails (3.3.0-1) unstable; urgency=medium

  [ Soren Stoutner ]
  * New upstream release (Closes: #1093928).
  * debian/copyright:
    - Update upstream copyright dates.
    - Add myself to the debian/* stanza.
  * debian/control:
    - Add myself as an uploader.
    - Remove obsolete XS-Ruby-Versions and XB-Ruby-Versions.
    - Remove version build-dependency on ruby-railties (<< 2:7.1).
    - Fix typo in description.
    - Bump standard version to 4.7.0 (no changes needed).
  * debian/patches/0001-gemspec-do-not-use-git.patch:  Refresh fuzz.
  * debian/patches/0002-Only-run-supported-Rails-test-apps.patch:  Refactor for
    new upstream release.
  * debian/salsa-ci.yml:  Update to currently recommended defaults.

  [ Debian Janitor ]
  * Remove constraints unnecessary since buster:
    + Build-Depends: Drop versioned constraint on ruby-railties.

 -- Soren Stoutner <soren@debian.org>  Mon, 10 Feb 2025 21:51:46 -0700

ruby-roadie-rails (3.0.0-1) unstable; urgency=medium

  * New upstream release.
  * Refresh patches
  * Update ruby-railties version constraint

 -- Lucas Kanashiro <kanashiro@debian.org>  Mon, 07 Feb 2022 12:23:13 -0300

ruby-roadie-rails (2.2.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
    - Adds support for Rails 6.1.

  [ Debian Janitor ]
  * Remove constraints unnecessary since stretch:
    + Build-Depends: Drop versioned constraint on ruby-roadie.
    + ruby-roadie-rails: Drop versioned constraint on ruby-roadie in Depends.
  * Bump debhelper from old 12 to 13.
  * Update standards version to 4.5.1, no changes needed.

  [ Daniel Leidert ]
  * d/control: Add Rules-Requires-Root field.
    (Standards-Version): Bump to 4.6.0.
    (Build-Depends): Bump railties version to <6.1. Add ruby-rspec-rails.
    (Depends): Remove interpreters and use ${ruby:Depends}.
  * d/copyright: Add Upstream-Contact field.
    (Copyright): Add team.
  * d/ruby-tests.rake: Add and enable tests.
  * d/rules: Use gem installation layout and install upstream changelog.
  * d/watch: Update to use github.
  * d/patches/0002-Only-run-supported-Rails-test-apps.patch: Add patch.
    - Disable tests for unsupported Rails versions.
  * d/patches/series: Enable new patch.
  * d/upstream/metadata: Add missing fields.

 -- Daniel Leidert <dleidert@debian.org>  Tue, 30 Nov 2021 07:00:44 +0100

ruby-roadie-rails (2.1.1-2) unstable; urgency=medium

  [ Debian Janitor ]
  * Use secure URI in Homepage field.
  * Bump debhelper from old 11 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

  [ Marc Dequènes (Duck) ]
  * Team upload to unstable.

 -- Marc Dequènes (Duck) <Duck@DuckCorp.org>  Mon, 03 Aug 2020 21:42:45 +0900

ruby-roadie-rails (2.1.1-1) experimental; urgency=medium

  * Team upload
  * New upstream version 2.1.1
  * Bump Standards-Version to 4.5.0 (no changes needed)
  * Drop compat file, rely on debhelper-compat and bump compat level to 12
  * Update dependency on rails
  * Refresh patch
  * Remove obsolete 0002-Relax-roadie-dependency-version.patch

 -- Sruthi Chandran <srud@debian.org>  Thu, 06 Feb 2020 16:35:23 +0100

ruby-roadie-rails (1.3.0-2) unstable; urgency=medium

  [ Utkarsh Gupta ]
  * Add salsa-ci.yml

  [ Lucas Kanashiro ]
  * Add patch to relax roadie dependency version
  * Declare compliance with Debian Policy 4.4.1

 -- Lucas Kanashiro <lucas.kanashiro@canonical.com>  Tue, 12 Nov 2019 09:04:21 -0300

ruby-roadie-rails (1.3.0-1) unstable; urgency=medium

  * Team upload.

  [ Cédric Boutillier ]
  * Use https:// in Vcs-* fields
  * Use https:// in Vcs-* fields
  * Bump Standards-Version to 3.9.7 (no changes needed)
  * Run wrap-and-sort on packaging files

  [ Antonio Terceiro ]
  * Remove myself from Uploaders:

  [ Lucas Kanashiro ]
  * New upstream version 1.3.0
  * Add myself to Uploaders list
  * Bump debhelper compatibility level to 11
  * Declare complaince with Debian Policy 4.2.0
  * Update VCS urls to point to salsa
  * debian/control: update ruby-railties version constraints
  * Update years of upstream copyright
  * debian/copyright: use secure url in Format and Sourece fields
  * Update Debian packaging copyright
  * Remove integration tests patch, the tests were removed in this upstream
    release
  * Refresh gemspec patch
  * debian/watch: update to use gemwatch.debian.net
  * Remove debian/ruby-tests.rake, no more tests to be executed

 -- Lucas Kanashiro <lucas.kanashiro@collabora.co.uk>  Fri, 24 Aug 2018 14:46:50 -0300

ruby-roadie-rails (1.1.0-1) unstable; urgency=medium

  * Initial release

 -- Antonio Terceiro <terceiro@debian.org>  Sun, 14 Feb 2016 14:02:43 -0200
