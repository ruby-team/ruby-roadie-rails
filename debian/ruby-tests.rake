require 'gem2deb/rake/spectask'

ENV.delete('CI')

Gem2Deb::Rake::RSpecTask.new do |spec|
  spec.pattern = './spec/**/*_spec.rb'
end
